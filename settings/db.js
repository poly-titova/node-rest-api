'use strict';

const mysql = require('mysql');

const connection = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'password',
  database: 'node-rest-api'
});

connection.connect((error) => {
  if (error) {
    return console.log(`Ошибка при подключении к бд: ${error}`);
  } else {
    return console.log('Подключение успешно');
  }
});

module.exports = connection;
